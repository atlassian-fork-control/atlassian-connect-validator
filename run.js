#!/usr/bin/env node

var validator = require('./validate'),
    util = require('util'),
    async = require('async'),
    request = require('request'),
    fs = require('fs'),
    S = require('string'),
    nomnom = require('nomnom'),
    c = require('colors');

nomnom.option('descriptor', {
    abbr: 'd',
    required: true,
    help: 'Descriptor file, may be a URL'
}).option('schema', {
    abbr: 's',
    required: true,
    help: 'Schema file'
});

var opts = nomnom.parse();

var parseJson = function (filename) {
    var parseFn = function (callback) {
        var result, error;
        try {
            result = JSON.parse(fs.readFileSync(filename, 'utf8'));
        } catch (e) {
            error = e;
        }
        callback(error, result);
    };

    return parseFn;
};

var downloadJson = function (uri) {
    var downloadFn = function (callback) {
        var opts = {
            json: true
        };
        console.log("Downloading " + uri.grey);
        request(uri, opts, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                callback(null, body);
            } else {
                callback(error, null);
            }
        });
    }
    return downloadFn;
};

var retriever = function (thingo) {
    if (S(thingo).startsWith('http')) {
        return downloadJson(thingo);
    } else {
        return parseJson(thingo);
    }
}

async.parallel({
    descriptor: function(callback){
        retriever(opts.descriptor)(callback);
    },
    schema: function(callback){
        retriever(opts.schema)(callback);
    }
},
function(err, result) {
    if (err) {
        console.log(err);
    } else {
        validator.validateDescriptor(result.descriptor, result.schema, function(errors) {
            if (!errors) {
                console.log("Validation passed");
            } else {
                console.log(util.inspect(errors, {
                    colors: true,
                    depth: 5
                }));
            }
        });
    }
});

